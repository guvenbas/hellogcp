package com.ag.helloGCP;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

	@Bean
	public GoogleCredentialFactory getCredential() {
		return new GoogleCredentialFactory();
	}
}
