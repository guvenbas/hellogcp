package com.ag.helloGCP;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.iam.v1.Iam;
import com.google.api.services.iam.v1.IamScopes;
import com.google.api.services.iam.v1.model.ListRolesResponse;
import com.google.api.services.iam.v1.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

@Service
public class IAMService {
	private GoogleCredentialFactory googleCredential;

	@Autowired
	public IAMService(GoogleCredentialFactory googleCredential) {
		this.googleCredential = googleCredential;
	}


	public List<Role> getRoles() {
		Iam iam = null;
		try {
			iam = new Iam.Builder(GoogleNetHttpTransport.newTrustedTransport(),
					JacksonFactory.getDefaultInstance(), googleCredential.getCredentialForService(IamScopes.CLOUD_PLATFORM)).setApplicationName("hello-gcp").build();
		} catch(GeneralSecurityException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}

		List<Role> roles = null;
		try {
			ListRolesResponse response = iam.roles().list().execute();
			roles = response.getRoles();
		} catch(IOException e) {
			e.printStackTrace();
		}


		return roles;
	}
}
