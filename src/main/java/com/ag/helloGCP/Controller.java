package com.ag.helloGCP;

import com.google.api.services.iam.v1.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class Controller {

	private IAMService iamService;

	@Autowired
	public Controller(IAMService iamService) {
		this.iamService = iamService;
	}

	@GetMapping("/UUID")
	public String getUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}

	@GetMapping("/rand")
	public String getRandomNumber() {
		return "random number:" + Math.random();
	}

	@GetMapping("/hello")
	public String sayHello() {
		return "hello budy!";
	}

	@GetMapping("/getenv")
	public String getWorkasEnv() {
		System.out.println("WORKAS:" + System.getenv("WORKAS"));
		System.out.println("TEST_ENV:" + System.getenv("TEST_ENV"));
		return System.getenv("WORKAS");
	}

	@GetMapping("/getRoles")
	public List<Role> getRoles() {
		return iamService.getRoles();
	}
}
