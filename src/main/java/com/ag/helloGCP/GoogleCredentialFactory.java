package com.ag.helloGCP;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.services.iam.v1.IamScopes;

import java.io.IOException;
import java.util.Collections;

public class GoogleCredentialFactory {


	public GoogleCredential getCredentialForService(String service) {
		GoogleCredential credential = null;
		try {
			credential = GoogleCredential.getApplicationDefault().createScoped(Collections.singleton(service));//IamScopes.CLOUD_PLATFORM
		} catch(IOException e) {
			e.printStackTrace();
		}
		return credential;
	}
}
